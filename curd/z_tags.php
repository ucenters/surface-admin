<?php
 return array (
  'table' => 'z_tags',
  'title' => '文章标签',
  'description' => '文章标签',
  'auto_timestamp' => true,
  'button_default' =>
  array (
    0 => 'create',
    1 => 'update',
    2 => 'delete',
    3 => 'refresh',
  ),
  'page' => true,
  'extend' =>
  array (
  ),
  'pk' => 'id',
  'button' =>
  array (
  ),
  'fields' =>
  array (
    'id' =>
    array (
      'title' => 'id',
      'field' => 'id',
      'default' => NULL,
      'weight' => 1,
      'search_type' => '_',
      'search' => '=',
      'search_extend' =>
      array (
      ),
      'table_type' => 'column',
      'table_format' =>
      array (
      ),
      'table_sort' => false,
      'table_extend' =>
      array (
      ),
      'form_type' => 'hidden',
      'marker' => '',
      'form_format' =>
      array (
      ),
      'form_extend' =>
      array (
      ),
      'save_format' =>
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' =>
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'title' =>
    array (
      'title' => '名称',
      'field' => 'title',
      'default' => '',
      'weight' => 15,
      'search_type' => 'input',
      'search' => 'LIKE',
      'search_extend' =>
      array (
      ),
      'table_type' => 'column',
      'table_format' =>
      array (
      ),
      'table_sort' => false,
      'table_extend' =>
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' =>
      array (
      ),
      'form_extend' =>
      array (
      ),
      'save_format' =>
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' =>
      array (
      ),
      'option_lang' => '',
      'option_relation' =>
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    )
  ),
);
