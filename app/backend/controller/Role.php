<?php

namespace app\backend\controller;

use app\api\annotation\illustrate\Group;
use app\api\annotation\illustrate\Route;
use app\backend\model\Role as RoleModel;
use app\backend\controller\role\
{
    Form,
    Assign,
    Search,
    Table};


class Role extends BackendController
{

    /**
     * @param Search $search
     *
     * @return array|string
     */
    public function search(Search $search)
    {
        return $this->createForm($search);
    }

    /**
     * @param Table $table
     *
     * @return array|string
     */
    public function index(Table $table)
    {
        return $this->createTable($table);
    }

    public function create(Form $form)
    {
        return $this->update($form);
    }

    /**
     * @param Form $form
     *
     * @return array|string
     */
    public function update(Form $form)
    {
        return $this->createForm($form);
    }

    /**
     * 分配权限
     *
     * @param Assign $assign
     *
     * @return array|string
     */
    public function assigns(Assign $assign)
    {
        return $this->createForm($assign);
    }

    public function change($id, $field, $value, RoleModel $model)
    {
        if (! in_array($field, ['title', 'status']))
        {
            return _error('No data submission');
        }
        $model->editField($field, $value, $id);

        return _success(['edit', 'success']);
    }


    public function delete($id)
    {
        RoleModel::destroy($id);

        return _success(['delete', 'success']);
    }

}
